const Customer = require("../models/customerModel");

exports.findAll = (req, res) => {
    Customer.getAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Something Error."
        });
      else res.send(data);
    });
};

exports.findOne = (req, res) => {
  Customer.findById(req.params.customerId, (err, data) => {
    if (err) {
      if (err.kind === "Not Found") {
        res.status(404).send({
          message: 'User Not Found With This Id'
        });
      } else {
        res.status(500).send({
          message: "Not Have Data With This Id"
        });
      }
    } else res.send(data);
  });
};



exports.create = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Required !"
    });
  }

  const customer = new Customer({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    gender: req.body.gender,
    isMaried: req.body.isMaried,
    address: req.body.address,
  });

  Customer.create(customer, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Something Error."
      });
    else res.send(data);
  });
};

exports.create = (req, res) => {
    if (!req.body) {
      res.status(400).send({
        message: "Required!"
      });
    }
    const customer = new Customer({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      gender: req.body.gender,
      isMaried: req.body.isMaried,
      address: req.body.address,
    });

    Customer.create(customer, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Something Error"
        });
      else res.send(data);
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Required!"
    });
  }
  Customer.updateById(
    req.params.customerId,
    new Customer(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "Not Found") {
          res.status(404).send({
            message: 'User Not Found With This Id'
          });
        } else {
          res.status(500).send({
            message: "Cant Update Data With Id"
          });
        }
      } else res.send(data);
    }
  );
};

exports.delete = (req, res) => {
  Customer.remove(req.params.customerId, (err, data) => {
    if (err) {
      if (err.kind === "Not Found") {
        res.status(404).send({
          message: 'cant find data with this id'
        });
      } else {
        res.status(500).send({
          message: "cant delete data with this id"
        });
      }
    } else res.send({ message: 'data has been deleted !' });
  });
};

exports.deleteAll = (req, res) => {
  Customer.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Something went wrong"
      });
    else res.send({ message: 'delete all data successed!' });
  });
}