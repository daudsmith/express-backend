module.exports = app => {
    const customer = require("../controllers/customerController");
 
    app.get("/customer", customer.findAll);
    app.get("/customer/:customerId", customer.findOne);
    app.post("/customer", customer.create);
    app.put("/customer/:customerId", customer.update);
    app.delete("/customer/:customerId", customer.delete);
    app.delete("/customer", customer.deleteAll);
  };