const sql = require("./db.js");

const Customer = function(customer) {
    this.name = customer.name;
    this.email = customer.email;
    this.password = customer.password;
    this.gender = customer.gender;
    this.isMaried = customer.isMaried;
    this.address = customer.address;
};

Customer.getAll = result => {
    sql.query("SELECT * FROM customer", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("customer: ", res);
      result(null, res);
    });
  };

Customer.findById = (customerId, result) => {
  sql.query(`SELECT * FROM customer WHERE id = ${customerId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      console.log("customer data: ", res[0]);
      result(null, res[0]);
      return;
    }
    result({ kind: "Not Found" }, null);
  });
};

Customer.create = (newCustomer, result) => {
  sql.query("INSERT INTO customer SET ?", newCustomer, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    console.log("Successed : ", { id: res.insertId, ...newCustomer});
    result(null, { id: res.insertId, ...newCustomer });
  });
};

Customer.updateById = (id, customer, result) => {
  sql.query(
    "UPDATE customer SET name = ?, email = ?, password = ?, gender = ?, isMaried = ?, address = ? WHERE id = ?",
    [customer.name, customer.email, customer.password, customer.gender, customer.isMaried, customer.address, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        result({ kind: "Data Not Found" }, null);
        return;
      }
      console.log("update data.. : ", { id: id, ...customer });
      result(null, { id: id, ...customer });
    }
  );
};

Customer.remove = (id, result) => {
  sql.query("DELETE FROM customer WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    if (res.affectedRows == 0) {
      result({ kind: "Not Found" }, null);
      return;
    }
    console.log("success delete id : ", id);
    result(null, res);
  });
};

Customer.removeAll = result => {
  sql.query("DELETE FROM customer", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log('Success ${res.affectedRows} customer');
    result(null, res);
  });
};

 
module.exports = Customer;